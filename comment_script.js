// comment_script.js

const axios = require('axios');
const html2canvas = require('html2canvas');

// Constants
const gitlabApiUrl = 'https://gitlab.com/api/v4';
const accessToken = process.env.ACCESS_TOKEN;
const commentText = `Your request is merged thanks for contributing\n`;
const commitMessage = process.env.CI_COMMIT_TITLE;
const projectId = process.env.CI_PROJECT_ID;

// Function to fetch merge request ID based on commit SHA
async function getMergeRequestID(branchName) {
    try {
        const url = `${gitlabApiUrl}/projects/${projectId}/merge_requests?source_branch=${branchName}&scope=all&state=merged`;
        const response = await axios.get(url, { headers: { 'Private-Token': accessToken } });

        if (response.data.length > 0) {
            return response.data[0].iid; // Return the merge request ID
        }
    } catch (error) {
        console.error('Error fetching merge request ID:', error.message);
    }

    return null; // Return null if merge request ID is not found
}

// Function to add comment to the merge request
async function addCommentToMergeRequest(mergeRequestId) {
    try {
        const cardHtml = `
            <div style="background-color: #f0f0f0; padding: 20px; border-radius: 10px; font-family: Arial;">
                <h2>Thank You Card</h2>
                <p>${commentText}</p>
            </div>
        `;
        const canvas = await html2canvas(document.createElement('div').innerHTML = cardHtml);
        // Convert canvas to base64 image
        const imageData = canvas.toDataURL('image/png');
    
        const response = await axios.post(`${gitlabApiUrl}/projects/${projectId}/merge_requests/${mergeRequestId}/notes`, {
            body: `![Thank You Card](data:image/png;base64,${imageData})`,
        }, {
            headers: {
                'Private-Token': accessToken,
            },
        });

        console.log('Comment added successfully:', response.data);
    } catch (error) {
        console.error('Error adding comment:', error);
    }
}

// Function to fetch merge request details
async function getMergeRequestDetails(mergeRequestId) {
    try {
        const response = await axios.get(`${gitlabApiUrl}/projects/${projectId}/merge_requests/${mergeRequestId}`, {
            headers: {
                'Content-Type': 'application/json',
                'Private-Token': accessToken
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching merge request details:', error);
        return null;
    }
}

// Function to fetch all merge requests by a user
async function getAllMergeRequestsByUser(userId) {
    try {
        const response = await axios.get(`${gitlabApiUrl}/projects/${projectId}/merge_requests?author_id=${userId}&per_page=100`, {
            headers: {
                'Content-Type': 'application/json',
                'Private-Token': accessToken
            }
        });
        return response.data;
    } catch (error) {
        console.error('Error fetching merge requests by user:', error);
        throw error;
    }
}


// Main function to handle the workflow
async function main() {
    // Parse commit message to extract branch name
    const branchRegex = /Merge branch '(.+?)' into '(.+?)'/;
    const match = commitMessage.match(branchRegex);

    if (match && match.length === 3) {
        const branchName = match[1];
        try {
            const mergeRequestId = await getMergeRequestID(branchName);
            if (!mergeRequestId) {
                throw (`Merge Request ID not found for branch '${branchName}'`);
            }

            console.log(`Merge Request ID for branch ${mergeRequestId}`);
            const mergeRequestDetails = await getMergeRequestDetails(mergeRequestId);
            if (!mergeRequestDetails) {
                throw ('merge req. details not found')
            }

            console.log('<<', mergeRequestDetails)
            const authorId = mergeRequestDetails.author.id;
            const mergeRequests = await getAllMergeRequestsByUser(authorId)
            console.log(`Total merge requests by user: ${mergeRequests.length}`);

            if (mergeRequests.length === 1) {
                console.log("This is the user's first merge request. Proceeding with the pipeline.");
            } else {
                console.log("This is not the user's first merge request. Exiting pipeline.");
            }
            await addCommentToMergeRequest(mergeRequestId);
        } catch (error) {
            console.error('Error:', error.message);
        }
    } else {
        console.error('Error: cannot find branch name');
    }
}

main();


// to-do
/*
1. check if is first commit?
2. If yes, create card.
*/